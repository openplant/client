using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using System.Data;
using System.IO;

namespace OpenPlant
{
    [ServiceContract]
    public interface iTestContract
    {
        [OperationContract]
        DateTime GetServerTimeUTC();

        [OperationContract]
        int ServerBackupTime();

        [OperationContract]
        Post GetPostData();

        [OperationContract]
        bool SendPostData(List<Post> ListPost) ;

        [OperationContract]
        string SendMessage(string msg);
    }

}
