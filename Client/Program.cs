﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using MySql.Data;
using MySql.Data.MySqlClient;

namespace OpenPlant
{
    class Program
    {
        static void Main(string[] args)
        {

            string someStringFromColumnZero = "";

            WCFClient<iTestContract> WcfClient = new WCFClient<iTestContract>("127.0.0.1", 33173);
            WcfClient.Start();
            DateTime ddd = WcfClient.Channel.GetServerTimeUTC();
            Post post = WcfClient.Channel.GetPostData();

            string msg = "asdfasd";
            string msgRecieved = "";
            msgRecieved =  WcfClient.Channel.SendMessage(msg);
            int ServerBackupTime = WcfClient.Channel.ServerBackupTime();
            Console.WriteLine("Server Time: " + ddd.ToString() + " Server Backup Time: " + ServerBackupTime + "Message Received by Client "+msgRecieved );
            Console.ReadLine();

            //Post post = new Post();

            ////Get data from db
            //var dbCon = DBConnection.Instance();
            //dbCon.DatabaseName = "eculight";
            //if (dbCon.IsConnect())
            //{
            //    //suppose col0 and col1 are defined as VARCHAR in the DB
            //    string query = "SELECT `post_content` FROM `wp_posts` WHERE `ID` = 48; ";
            //    var queryExecution = new MySqlCommand(query, dbCon.Connection);
            //    var result = queryExecution.ExecuteReader();
            //    while (result.Read())
            //    {
            //        someStringFromColumnZero = result.GetString(0);
            //    }
            //}
            //post.post_name = someStringFromColumnZero;
            //Console.WriteLine(post.post_name);
            Console.ReadLine();
        }
    }

    public class DBConnection
    {
        private DBConnection()
        {
        }

        private string databaseName = string.Empty;
        public string DatabaseName
        {
            get { return databaseName; }
            set { databaseName = value; }
        }

        public string Password { get; set; }
        private MySqlConnection connection = null;
        public MySqlConnection Connection
        {
            get { return connection; }
        }

        private static DBConnection _instance = null;
        public static DBConnection Instance()
        {
            if (_instance == null)
                _instance = new DBConnection();
            return _instance;
        }

        public bool IsConnect()
        {
            bool result = true;
            if (Connection == null)
            {
                if (String.IsNullOrEmpty(databaseName))
                    result = false;
                string connstring = string.Format("Server=localhost; database={0}; UID=root; password=", databaseName);
                connection = new MySqlConnection(connstring);
                connection.Open();
                result = true;
            }

            return result;
        }

        public void Close()
        {
            connection.Close();
        }
    }


    public class Post
    {
        public string post_name { get; set; }
        public string post_title { get; set; }
    }
}
